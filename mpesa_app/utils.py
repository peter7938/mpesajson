import pikepdf
from copy import copy
import PyPDF2
import io
import re
from io import StringIO, BytesIO
import re
from PyPDF2 import PdfFileReader
from openpyxl import Workbook
from openpyxl.styles import Font, Color, colors
import random, string
from openpyxl.writer.excel import save_virtual_workbook
import openpyxl
from openpyxl import load_workbook
import os
import pandas as pd
from pandas import Series
import pandas.io.formats.excel
pandas.io.formats.excel.header_style = None
import xlsxwriter
from flask import make_response
import json
import numpy as np
from mpesa_app import app

i = 0
#regex pattern for mpesa statement
regex1 = r'(C.+ Name)(.+)(M.+ Number)(\d+)(E\w+ Address)(.+)(D[ a-zA-Z]+Statement)(.+)(S.+ Period)(.+ - \d{2} \w+ \d{4})'
regex = r'(\w{10})(\d{4}-\d{2}-\d{2} \d{2}\:\d{2}\:\d{2})(.+?)(Completed)(.*?\.\d{2})(.*?\.\d{2})'


def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['pdf'])
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def extract_from_pdf(file, password):
    #decrypting the encrypted pdf file
    content = pikepdf.open(file, password=password)
    inmemory_file = BytesIO()
    content.save(inmemory_file)
    #reading and extracting data from the decrypted pdf file
    pdf_reader = PyPDF2.PdfFileReader(inmemory_file)
    num_pages = pdf_reader.getNumPages()
    extracted_data = StringIO()
    for page in range(num_pages):
        extracted_data.writelines(pdf_reader.getPage(page).extractText())

    return num_pages, extracted_data


def random_str(length=8):
	s = ''
	for i in range(length):
		s += random.choice(string.ascii_letters + string.digits)

	return s

def parse_mpesa_content(extracted_data):
    extracted_data.seek(0)
    lines = extracted_data.read()
    matches = re.compile(regex).findall(lines)
    matches2 = re.compile(regex1).findall(lines)

    fb = Font(name='Calibri', color=colors.BLACK, bold=True, size=11, underline='single')
    i = 0
    #creating the spreadsheet
    book = Workbook()
	# grab the active worksheet
    sheet = book.active
	#excel styling 2
    ft = Font(name='Calibri', color=colors.BLUE, bold=True, size=11, underline='single')

    sheet['A1'] = 'Receipt_no'
    sheet['B1'] = 'Transaction_date'
    sheet['C1'] = 'Transaction_details'
    sheet['D1'] = 'Transaction_status'
    sheet['E1'] = 'Transaction_amount'
    sheet['F1'] = 'Account_balance'

    a1 = sheet['A1']
    b1 = sheet['B1']
    c1 = sheet['C1']
    d1 = sheet['D1']
    e1 = sheet['E1']
    f1 = sheet['F1']

    a1.font = ft
    b1.font = ft
    c1.font = ft
    d1.font = ft
    e1.font = ft
    f1.font = ft


	#adding every match to the excel file
    while i < len(matches):
	    # print(matches[i])
        sheet.append(matches[i])
        i = i + 1

    filename = random_str()
    book.save(filename)
    f = open(filename, 'rb')
    file = BytesIO(f.read())
    f.close()
    os.remove(filename)

    return file, matches2

def output_json(workbook, number):
    excel_df = pd.read_excel(workbook)
    excel_df['Transaction_amount'] = excel_df['Transaction_amount'].astype(str).str.replace(',', '').astype(float)
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Withdrawal Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Customer Transfer of Funds Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Pay Utility Reversal by Safaricom', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('M-Shwari Withdraw', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Pay Bill Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('OD Loan Repayment to 232323 - M-PESA Overdraw', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Pay Merchant Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Customer Send Money To Unregistered User Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('IMT Send Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Charge', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('Reversal', regex=True)]
    excel_df = excel_df[~excel_df['Transaction_details'].str.contains('reversal', regex=True)]


    def withdrawn(excel_df):
        withdrawal = excel_df[excel_df['Transaction_amount']<0]
        withdrawal['Transaction_amount'] = withdrawal['Transaction_amount'].astype(str).str.replace('-', '').astype(float)
        withdrawal['Transaction_type'] = withdrawal['Transaction_details']
        a1 = withdrawal[withdrawal['Transaction_details'].str.contains('Fuliza M-Pesa', regex=True)]
        withdrawal = withdrawal[~withdrawal['Transaction_details'].str.contains('Fuliza M-Pesa', regex=True)]
        return withdrawal, a1
    withdrawal, a1 = withdrawn(excel_df)

    def paid_in(excel_df):
        paidin = excel_df[excel_df['Transaction_amount']>0]
        b1 = paidin[paidin['Transaction_details'].str.contains('OverDraft of Credit Party', regex=True)]
        return paidin, b1
    paid, b1 = paid_in(excel_df)

    a1['Fuliza_amount'] = np.where(a1['Receipt_no'].reset_index(drop=True) == b1['Receipt_no'].reset_index(drop=True), b1['Transaction_amount'], '0')
    withdrawal = withdrawal.append(a1)
    withdrawal['Fuliza_amount'] = withdrawal['Fuliza_amount'].fillna("0")

    def format(row):
        index = None
        reg = re.search(r'(\w+\s*\w+)(\s)', row['Transaction_details'])
        if reg:
            index = reg.end()
            row['Transaction_details'] = row['Transaction_details'][:index]

        return row

    sorted_df = withdrawal.apply(format, axis=1).groupby(['Transaction_details'], as_index=False)
    sorted_df1 = paid.apply(format, axis=1).groupby(['Transaction_details'], as_index=False)

    def format1(row):
        index = None
        reg = re.search(r'([0-9]\d*[0-9])', row['Transaction_details'])
        if reg:
            index = reg.end()
            row['Transaction_details'] = row['Transaction_details'][:index]

        return row

    def format2(row):
        index = None
        reg = re.search(r'([0-9]\d*[0-9])', row['Transaction_details'])
        if reg:
            index = reg.start()
            row['Transaction_details'] = row['Transaction_details'][index:]

        return row

    sorted_df2 = withdrawal.apply(format1, axis=1)
    sorted_df3 = sorted_df2.apply(format2, axis=1)
    sorted_df4 = paid.apply(format1, axis=1)
    sorted_df5 = sorted_df4.apply(format2, axis=1)
    #sorted_df3 = paid.apply(format, axis=1).groupby(['Transaction_details'], as_index=False).apply(lambda r: r)

    def func_withdrawal(x):
        if 'Pay Bill' in x:
            return 'Business Number'
        elif 'Agent' in x:
            return 'Withdraw'
        elif 'Withdrawal at' in x:
            return 'Withdraw'
        elif 'Airtime' in x:
            return "Airtime/Bundles"
        elif 'Merchant Payment' in x:
            return 'Till/Merchant'
        elif 'Customer Transfer to' in x:
            return 'Money Transfer'
        elif 'Customer Transfer Fuliza M-Pesa to' in x:
            return 'Money Transfer'
        elif 'M-Shwari Loan Repayment' in x:
            return 'Loan'
        elif 'M-Shwari Deposit' in x:
            return 'Savings'
        elif 'KCB' in x:
            return 'Loan'
        elif 'Bundles' in x:
            return 'Airtime/Bundles'
        elif 'Transfer of funds to unregistered user' in x:
            return 'Money Transfer'
        elif 'Transfer of funds Fuliza M-Pesa to unregistered user' in x:
            return 'Money Transfer'
        elif 'B2C Reversal by M-PESA' in x:
            return 'Till/Merchant'
        elif 'Customer Send Money To Unregistered User' in x:
            return 'Money Transfer'
        elif 'Send Money Abroad to' in x:
            return 'Money Transfer'
        elif 'Offnet C2B Transfer to 331871 - SAFARICOM-TELKOMINTEROPERABILITY for Mobile' in x:
            return 'Money Transfer'
        elif 'M-Shwari Lock Activate and Save' in x:
            return 'Savings'
        elif 'M-Shwari Lock Deposit' in x:
            return 'Savings'
        elif 'M-Kesho Deposit' in x:
            return 'Savings'
        else:
            app.logger.info('This is it! %s', x)
            raise Exception(x)

    def func_paidin(x):
        if 'Funds received from' in x:
            return 'Money Transfer'
        elif 'Send Money Reversal via API from' in x:
            return 'Money Transfer'
        elif 'Agent' in x:
            return 'Deposit'
        elif 'Payment from' in x:
            return 'Business Number'
        elif 'M-Shwari Loan Disburse' in x:
            return 'Loan'
        elif 'M-Shwari Withdraw' in x:
            return 'Savings'
        elif 'Savings Client Online Withdrawal Request' in x:
            return 'Savings'
        elif 'KCB' in x:
            return 'Loan'
        elif 'Buy Bundles' in x:
            return 'Airtime/Bundles'
        elif 'Airtime' in x:
            return "Airtime/Bundles"
        elif "Transfer from Bank" in x:
            return 'Business Number'
        elif "Receive International Transfer From" in x:
            return 'Business Number'
        elif "Pay Utility Reversal by M-PESA" in x:
            return 'Money Transfer'
        elif 'Customer Transfer Reversal by M-PESA' in x:
            return 'Money Transfer'
        elif 'Receive International Zero Rated Transfer From 295028- Safaricom International Money Transfer' in x:
            return 'Money Transfer'
        elif 'Receive funds from' in x:
            return 'Business Number'
        elif "Receive International Zero Rated Transfer From 264702- WORLD REMIT INBOUND" in x:
            return 'Money Transfer'
        elif "Offnet B2C Transfer by 966888 - AIRTEL MONEY viaAPI" in x:
            return "Money Transfer"
        else:
            app.logger.info('This is it!%s', x)
            raise Exception(x)


    def f(entry):
        x = entry['Transaction_type']
        y = entry['Transaction_details']
        if 'Business Number' in x:
            reg1 = re.search('(?<= -).+', y)
            return reg1[0].strip()
        elif 'Till/Merchant' in x:
            reg2 = re.search('(?<= -).+', y)
            return reg2[0].strip()
        else:
            return ''

    def f1(x):
        a = x['Transaction_details']
        space = re.findall("  +", a)
        if space:
            r = re.sub(r"  +", ' ', a).strip()
            return r
        else:
            return a

    def fuliza_withdrawal(x):
        a = x['Transaction_details']
        b = x['Transaction_amount']
        if 'Fuliza M-Pesa' in a:
            return b
        else:
            return '0'

    def bparty(x):
        a = x['Transaction_details']
        reg = re.search('\d+', a)
        if reg:
            return a
        elif 'M-Shwari' in a:
            return 'Mshwari'
        elif 'KCB' in a:
            return 'KCB-Mpesa'
        else:
            return ''

    for ind, row in withdrawal.iterrows():
        withdrawal['Destination_id'] = sorted_df3['Transaction_details']
        # replace the matching strings
        df_updated = withdrawal[["Destination_id"]].replace(to_replace ='([a-zA-Z][\s\\\\]*)+', value = '', regex=True)
        withdrawal['B_party'] = sorted_df3.apply(bparty, axis=1)
        withdrawal['Transaction_type'] = withdrawal['Transaction_details'].apply(func_withdrawal)
        withdrawal['Destination_id'] = sorted_df3['Transaction_details']
        withdrawal['msisdn'] = number
        withdrawal['Direction'] = 'Sent'
        #withdrawal['Fuliza_amount'] = withdrawal.apply(fuliza_withdrawal, axis=1)

    for ind, row in paid.iterrows():
        paid['msisdn'] = number
        # replace the matching strings
        df_updated1 = paid[["msisdn"]].replace(to_replace ='([a-zA-Z][\s\\\\]*)+', value = '', regex=True)
        paid['msisdn'] = df_updated1
        paid = paid[~paid['Transaction_details'].str.contains('OverDraft of Credit Party', regex=True)]
        paid['Transaction_type'] = paid['Transaction_details'].apply(func_paidin)
        paid['Destination_id'] = number
        paid['Direction'] = 'Received'
        paid['Fuliza_amount'] = '0'
        paid['B_party'] = sorted_df5.apply(bparty, axis=1)


    #withdrawal['Transaction_details'] = withdrawal.apply(f, axis=1)
    #paid['Transaction_details'] = paid.apply(f, axis=1)
    z = withdrawal.append(paid, ignore_index=True)
    z['Transaction_details'] = z.apply(f, axis=1)
    z['Transaction_details'] = z.apply(f1, axis=1)
    z1 = z[['msisdn', 'Transaction_date', 'Transaction_details', 'Transaction_type', 'Transaction_amount', 'Account_balance', 'Fuliza_amount', 'Direction', 'B_party']].sort_values('Transaction_details').groupby(['Transaction_details'], as_index=False)
    z2 = list(z1)
    dfObj = pd.DataFrame(columns=['msisdn', 'Transaction_date', 'Transaction_details', 'Transaction_type', 'Transaction_amount', 'Account_balance', 'Fuliza_amount', 'Direction', 'B_party'])

    i = 0
    j = 0
    dfs = []
    dfs2 = []
    while i < len(z2):
        dfs.append(z2[i][1])
        i = i + 1

    while j < len(dfs):
        dfs2.append(dfs[j])
        j = j + 1

    #dfObj.append(dfs2, ignore_index=True)

    return dfs2, dfObj


def json1(workbook, name):
    x, y = output_json(workbook, name)
    d1 = y.append(x, ignore_index = True)
    d1['Fuliza_amount'] = d1['Fuliza_amount'].astype(str).str.replace(',', '').astype(float)
    d1['Account_balance'] = d1['Account_balance'].astype(str).str.replace(',', '').astype(float)
    d1_json = d1.to_json(orient='records')

    return d1_json

def find_name(matches2):
    for match in matches2:
        x = match[3]

    return x

def process_pdf(file, password):
    error = ''
    try:
        numpages, txtfile = extract_from_pdf(file, password)
    except Exception:
        error = 'Check file input and password!'
    else:
        content, matches2 = parse_mpesa_content(txtfile)
        if matches2:
            name = find_name(matches2)
            title = name
        if content:
            #content2 = exec_analytics(content)
            #pandas operation
            json_combination = json1(content, title)
            resp = make_response((json_combination, {
                'Content-Type': 'application/json',
                'Content-Disposition': 'attachment; filename={}.json'.format(title)
            }))
        else:
            resp =  make_response({'response' : content}, {
                'Content-Type': 'application/json',
            })
        return resp
