from flask import Flask, request, redirect, jsonify
import os
import urllib.request
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'C:/Users/PETER/Desktop/mpesajson/mpesa_app/files'

app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'

from mpesa_app import routes
