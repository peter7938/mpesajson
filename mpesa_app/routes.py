from mpesa_app import app
from flask import render_template, url_for, request, make_response, jsonify
from mpesa_app.utils import (extract_from_pdf, parse_mpesa_content,
                            output_json, allowed_file, process_pdf, json1)
import datetime
from functools import wraps
import os
import urllib.request
from werkzeug.utils import secure_filename


@app.route('/file-upload', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'file' not in request.files:
        resp = jsonify({'message' : 'No file part in the request'})
        resp.status_code = 400
        return resp
    file = request.files['file']
    password = request.form.get('password')
    if file.filename == '':
        resp = jsonify({'message' : 'No file selected for uploading'})
        resp.status_code = 400
        return resp
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        #get file name
        x = process_pdf(file, password)
        resp = jsonify({'message' : 'File successfully uploaded'})
        resp.status_code = 201
        return x
    else:
        resp = jsonify({'message' : 'Allowed file types are pdf'})
        resp.status_code = 400
        return resp
